#ifndef COMPOSITE_HPP_
#define COMPOSITE_HPP_

#include <iostream>
#include <string>
#include <list>
#include <memory>
#include <functional>

// "Component" 
class Component
{
protected:
	std::string name_;
public:
	Component(const std::string& name) :
		name_(name)
	{
	}

    Component(const Component&) = default;
    Component& operator=(const Component&) = default;

    virtual void display(int depth) = 0;
	
	virtual ~Component() 
	{
        std::cout << "~Component(name: " << name_ << ")\n";
	}
};

// "Composite" 
class Composite : public Component
{
private:
    std::list<std::shared_ptr<Component>> children;
public:
	Composite(const std::string& name) :
		Component(name)
	{
	}

    void add(std::shared_ptr<Component> c)
	{
		children.push_back(c);
	}

    void remove(std::shared_ptr<Component> c)
	{
		children.remove(c);
	}

	void display(int depth) override
	{
		std::cout << std::string(depth, '-') << name_ << std::endl;

        for(const auto& child : children)
            child->display(depth+2);
	}
	
	~Composite()
	{
	}
};

// "Leaf"
class Leaf : public Component
{
public:
	Leaf(const std::string& name) :
		Component(name)
	{
	}

	void display(int depth) override
	{
		std::cout << std::string(depth, '-') << name_ << std::endl;
	}
};

#endif /*COMPOSITE_HPP_*/
