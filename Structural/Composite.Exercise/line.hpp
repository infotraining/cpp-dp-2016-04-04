#ifndef LINE_HPP
#define LINE_HPP

#include "shape.hpp"

namespace Drawing
{

class Line : public ShapeBase
{
	Point end_of_line_;
public:
	Line(int x1 = 0, int y1 = 0, int x2 = 0, int y2 = 0) : ShapeBase(x1, y1), end_of_line_(x2, y2)
	{
	}

	Point endpoint() const
	{
		return end_of_line_;
	}

	void set_endpoint(const Point& pt)
	{
		end_of_line_ = pt;
	}

	virtual void draw() const
	{
		std::cout << "Drawing a line from: " << point() << " to: " << end_of_line_ << std::endl;
	}

	virtual Line* clone() const
	{
		return new Line(*this);
	}

	virtual void read(std::istream& in)
	{
		Point pt1, pt2;

		in >> pt1 >> pt2;

		set_point(pt1);
		set_endpoint(pt2);
	}

	virtual void write(std::ostream& out)
	{
		out << "Line " << point() << " " << endpoint() << std::endl;
	}
};

}

#endif
