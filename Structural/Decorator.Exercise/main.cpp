#include <memory>
#include "coffeehell.hpp"

void client(CoffeePtr coffee)
{
    std::cout << "Description: " << coffee->get_description() << "; Price: " << coffee->get_total_price() << std::endl;
    coffee->prepare();
}

class CoffeeBuilder
{
    CoffeePtr coffee_;
public:
    template <typename TCoffee>
    CoffeeBuilder& create_base()
    {
        coffee_ = std::make_unique<TCoffee>();

        return *this;
    }

    template <typename TCondiment>
    CoffeeBuilder& add()
    {
        coffee_ = std::make_unique<TCondiment>(std::move(coffee_));

        return *this;
    }

    CoffeePtr get_coffee()
    {
        return std::move(coffee_);
    }
};

int main()
{
    using namespace std;

    std::unique_ptr<Coffee> cf = make_unique<Whipped>(
                                     make_unique<Whisky>(
                                        make_unique<Whisky>(
                                            make_unique<Whisky>(
                                                make_unique<ExtraEspresso>(
                                                    make_unique<Espresso>())))));

    client(std::move(cf));

    std::cout << "\n\n";

    CoffeeBuilder cb;
    cb.create_base<Espresso>().add<ExtraEspresso>().add<Whisky>();
    cb.add<Whipped>();

    cf = cb.get_coffee();

    client(std::move(cf));
}
