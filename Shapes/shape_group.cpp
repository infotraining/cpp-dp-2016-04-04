#include "shape_group.hpp"
#include "shape_factory.hpp"

namespace
{
    using namespace Drawing;

    bool is_factory_registered =
            SingletonShapeFactory::instance()
                .register_creator("ShapeGroup", [] { return std::make_unique<ShapeGroup>();});

    bool is_rw_factory_registered =
            SingletonShapeRWFactory::instance()
                .register_creator("ShapeGroup", [] { return std::make_unique<ShapeGroupReaderWriter>();});
}
