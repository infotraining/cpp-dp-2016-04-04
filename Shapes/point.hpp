#ifndef POINT_HPP
#define POINT_HPP

namespace Drawing
{
    struct Point
    {
        int x;
        int y;

        Point(int x = 0, int y = 0) : x{x}, y{y}
        {}

        bool operator==(const Point& pt) const
        {
            return x == pt.x && y == pt.y;
        }

        bool operator!=(const Point& pt) const
        {
            return !(*this == pt);
        }
    };

    inline std::ostream& operator<<(std::ostream& out, const Point& pt)
    {
        out << "[" << pt.x << "," << pt.y << "]";
        return out;
    }

    inline std::istream& operator>>(std::istream& in, Point& pt)
    {
        char start, separator, end;
        int x, y;

        if ( in >> start && start != '[')
        {
            in.unget();
            in.clear(std::ios_base::failbit);
            return in;
        }

        in >> x >> separator >> y >> end;

        if ( !in || separator != ',' || end != ']' )
            throw std::runtime_error("Stream reading error");

        pt.x = x;
        pt.y = y;

        return in;
    }
}

#endif // POINT_HPP
