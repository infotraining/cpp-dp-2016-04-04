#define CATCH_CONFIG_MAIN

#include <algorithm>
#include "catch.hpp"
#include "../shape.hpp"
#include "../circle.hpp"
#include "../rectangle.hpp"
#include "../square.hpp"


using namespace std;
using namespace Drawing;

class ShapeImpl : public ShapeBase
{
public:
    using ShapeBase::ShapeBase; // inheritance of constructors

    void draw() const override
    {
    }   

    std::unique_ptr<IShape> clone() const override
    {
        return std::make_unique<ShapeImpl>(*this);
    }

    void accept(ShapeVisitor& v) override
    {

    }
};

TEST_CASE("Point")
{
    Point pt1{1, 2};
    Point pt2{1, 2};
    Point pt3{1, 3};

    SECTION("operator ==")
    {
        REQUIRE(pt1 == pt2);
        REQUIRE(!(pt1 == pt3));
    }

    SECTION("operator !=")
    {
        REQUIRE(!(pt1 != pt2));
        REQUIRE(pt1 != pt3);
    }

    SECTION("operator<<")
    {
        stringstream ss;

        ss << pt1;

        REQUIRE(ss.str() == "[1,2]");
    }

    SECTION("operator>>")
    {
        stringstream ss("[1,2]");

        Point pt;
        ss >> pt;

        REQUIRE(pt == Point(1, 2));
    }
}

TEST_CASE("Shape - construction")
{
    SECTION("when default created coordinates are set to zero")
    {
        ShapeImpl s;

        REQUIRE(s.coord() == Point(0, 0));
        REQUIRE(s.coord().x == 0);
        REQUIRE(s.coord().y == 0);
    }

    SECTION("when created with initial data it sets coordinates")
    {
        ShapeImpl s(10, 20);

        REQUIRE(s.coord() == Point(10, 20));
    }
}

TEST_CASE("Shape - move")
{
    ShapeImpl s(10, 20);

    SECTION("moving changes coordinates")
    {
        s.move(1, 2);

        REQUIRE(s.coord() == Point(11, 22));
    }
}

TEST_CASE("Circle")
{
    SECTION("default construction")
    {
        Circle c;

        REQUIRE(c.coord() == Point(0, 0));
        REQUIRE(c.radius() == 0);
    }

    SECTION("construction with data")
    {
        Circle c(10, 20, 30);

        REQUIRE(c.coord() == Point(10, 20));
        REQUIRE(c.radius() == 30);
    }
}

TEST_CASE("Rectangle")
{
    SECTION("default construction")
    {
        Drawing::Rectangle r{};

        REQUIRE(r.coord() == Point(0, 0));
        REQUIRE(r.width() == 0);
        REQUIRE(r.height() == 0);
    }

    SECTION("construction with data")
    {
        Drawing::Rectangle r{10, 20, 30, 40};

        REQUIRE(r.coord() == Point(10, 20));
        REQUIRE(r.width() == 30);
        REQUIRE(r.height() == 40);
    }

    SECTION("setter for width")
    {
        Drawing::Rectangle r{10, 20, 1, 2};

        r.set_width(10);
        REQUIRE(r.width() == 10);
        REQUIRE(r.height() == 2);
    }

    SECTION("setter for height")
    {
        Drawing::Rectangle r{10, 20, 1, 2};

        r.set_height(10);
        REQUIRE(r.width() == 1);
        REQUIRE(r.height() == 10);
    }

    SECTION("calculate area")
    {
        Drawing::Square r{10, 20};

        r.set_size(10);

        REQUIRE(r.area() == 100);
    }
}

TEST_CASE("RectangleReaderWriter")
{
    SECTION("sets properties from stream")
    {
        SECTION("read sets properties from stream")
        {
            stringstream ss{"[10,20] 30 40"};
            Drawing::Rectangle r;
            Drawing::RectangleReaderWriter rw;

            rw.read(ss, r);

            REQUIRE(r.coord() == Point(10, 20));
            REQUIRE(r.width() == 30);
            REQUIRE(r.height() == 40);
        }
    }
}
