#include "catch.hpp"

#include <string>
#include <fstream>
#include "../graphics_document.hpp"
#include "../circle.hpp"
#include "../rectangle.hpp"
#include "../shape_parser.hpp"


using namespace std;
using namespace Drawing;

class TestableFileShapeParser : public PlainTextShapeParser
{
    string content_;
    stringstream ss_;
protected:
    istream& open_stream(const string& file_name) override
    {
        stream_opened = file_name;
        return ss_;
    }
public:
    string stream_opened = "";

    TestableFileShapeParser(IShapeFactory& sf, IShapeReaderWriterFactory& srwf, const string& content) :
        PlainTextShapeParser{sf, srwf}, content_{content}, ss_{content_}
    {
    }
};

TEST_CASE("ShapeParser")
{
    string file_name = "test.txt";

    string content = R"(Circle [10,20] 30
Rectangle [30,20] 100 200)";


    SECTION("parsing file")
    {
        ShapeFactory& sf = SingletonShapeFactory::instance();
        ShapeRWFactory& srwf = SingletonShapeRWFactory::instance();
        bootstrap_shape_factory(sf);
        bootstrap_shape_rw_factory(srwf);
        TestableFileShapeParser mqFileParser(sf, srwf, content);
        auto result = mqFileParser.parse("test.txt");

        SECTION("opens a stream")
        {
            mqFileParser.stream_opened = file_name;
        }

        SECTION("parses a content")
        {
            REQUIRE(result.size() == 2);
            REQUIRE(dynamic_cast<Circle*>(result[0].get()) != nullptr);
            REQUIRE(dynamic_cast<Rectangle*>(result[1].get()) != nullptr);
        }
    }
}
