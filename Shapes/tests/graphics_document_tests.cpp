#include "catch.hpp"

#include <string>
#include <vector>

#include "../circle.hpp"
#include "../square.hpp"
#include "../graphics_document.hpp"

using namespace std;
using namespace Drawing;

class MockShapeFileParser : public IShapeParser
{
public:
    bool parse_called {false};
    string file_parsed = "";

    vector<ShapePtr> parse(const std::string& file_name) override
    {
        parse_called = true;
        file_parsed = file_name;

        vector<ShapePtr> shapes;
        shapes.push_back(make_unique<Circle>(10, 20, 20));
        shapes.push_back(make_unique<Square>(20, 30, 20));

        return shapes;
    }
};

TEST_CASE("GraphicsDocument")
{
    SECTION("openning file")
    {
        MockShapeFileParser parser;
        GraphicsDocument doc(parser);
        auto file_to_parse = "drawing.txt";

        SECTION("calls parse on parser")
        {
            doc.open(file_to_parse);

            REQUIRE(parser.parse_called);
            REQUIRE(parser.file_parsed == file_to_parse);
        }

        SECTION("sets shape in document")
        {
            doc.open(file_to_parse);

            REQUIRE(doc.size() == 2);
        }
    }
}
