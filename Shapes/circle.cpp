#include "circle.hpp"
#include "shape_factory.hpp"
#include <memory>

namespace
{
    using namespace Drawing;

    bool is_factory_registered =
            SingletonShapeFactory::instance()
                .register_creator("Circle", [] { return std::make_unique<Circle>();});

    bool is_rw_factory_registered =
            SingletonShapeRWFactory::instance()
                .register_creator("Circle", [] { return std::make_unique<CircleReaderWriter>();});
}
