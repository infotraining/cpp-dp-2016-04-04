#ifndef SHAPE_PARSER_HPP
#define SHAPE_PARSER_HPP

#include <fstream>
#include "shape_factory.hpp"

namespace Drawing
{
    class IShapeParser
    {
    public:
        virtual ~IShapeParser() = default;
        virtual std::vector<ShapePtr> parse(const std::string& file_name) = 0;
    };

    class PlainTextShapeParser : public IShapeParser
    {
        IShapeFactory& shape_factory_;
        IShapeReaderWriterFactory& shape_rw_factory_;
        std::fstream fin;
    public:
        PlainTextShapeParser(IShapeFactory& shape_factory, IShapeReaderWriterFactory& shape_rw_factory)
            : shape_factory_{shape_factory}, shape_rw_factory_{shape_rw_factory}
        {
        }

        std::vector<ShapePtr> parse(const std::string &file_name) override
        {
            std::vector<ShapePtr> shapes;

            std::istream& in_file = open_stream(file_name);

            while (!in_file.eof())
            {
                std::string id;
                in_file >> id;
                auto shape = shape_factory_.create(id);
                auto rw = shape_rw_factory_.create(id);
                rw->read(in_file, *shape);
                shapes.push_back(move(shape));
            }

            return shapes;
        }

    protected:
        virtual std::istream& open_stream(const std::string& file_name)
        {
            fin.open(file_name);

            return fin;
        }
    };
}

#endif // SHAPE_PARSER_HPP
