#ifndef SHAPE_READER_WRITER_HPP
#define SHAPE_READER_WRITER_HPP

#include "shape.hpp"
#include <memory>

namespace Drawing
{
    class IShapeReaderWriter
    {
    public:
        virtual ~IShapeReaderWriter() = default;
        virtual void read(std::istream& in, IShape& shape) = 0;
        virtual void write(std::ostream& out, IShape& shape) = 0;
    };

    using ShapeReaderWriterPtr = std::unique_ptr<IShapeReaderWriter>;

    class IShapeReaderWriterFactory
    {
    public:
        virtual ~IShapeReaderWriterFactory() = default;
        virtual ShapeReaderWriterPtr create(const std::string& id) = 0;
    };
}

#endif // SHAPE_READER_WRITER_HPP
