#ifndef GENERIC_FACTORY_HPP
#define GENERIC_FACTORY_HPP

#include <string>
#include <unordered_map>
#include <functional>

template <typename ProductType, typename IdType = std::string, typename CreatorType = std::function<ProductType()>>
class GenericFactory
{
    std::unordered_map<IdType, CreatorType> creators_;
public:
    bool register_creator(const IdType& id, CreatorType&& creator)
    {
        return creators_.insert(std::make_pair(id, std::move(creator))).second;
    }

    ProductType create_object(const IdType& id) const
    {
        try
        {
            auto& creator = creators_.at(id);
            return creator();
        }
        catch(const std::out_of_range& e)
        {
            throw std::runtime_error("Bad shape id: " + id);
        }
    }
};

#endif // GENERIC_FACTORY_HPP
