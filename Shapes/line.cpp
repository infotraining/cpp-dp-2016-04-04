#include "line.hpp"
#include "shape_factory.hpp"

namespace
{
    using namespace Drawing;

    bool is_factory_registered =
            SingletonShapeFactory::instance()
                .register_creator("Line", [] { return std::make_unique<Line>();});

    bool is_rw_factory_registered =
            SingletonShapeRWFactory::instance()
                .register_creator("Line", [] { return std::make_unique<LineReaderWriter>();});
}
