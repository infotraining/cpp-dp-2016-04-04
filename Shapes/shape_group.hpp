#ifndef SHAPEGROUP_H
#define SHAPEGROUP_H

#include <vector>
#include "shape.hpp"
#include "shape_reader_writer.hpp"
#include "shape_factory.hpp"

namespace Drawing
{
    class ShapeGroup : public IShape
    {
        std::vector<ShapePtr> shapes_;
        using ShapeIterator = typename std::vector<ShapePtr>::const_iterator;
    public:
        ShapeGroup() = default;

        ShapeGroup(const ShapeGroup& source)
        {
            for(const auto& s : source.shapes_)
                shapes_.push_back(s->clone());
        }

        ShapeGroup& operator=(const ShapeGroup& source)
        {
            if (this != &source)
            {
                shapes_.clear();

                for(const auto& s : source.shapes_)
                    shapes_.push_back(s->clone());
            }

            return *this;
        }

        void add_shape(ShapePtr shape)
        {
            shapes_.push_back(std::move(shape));
        }

        void move(int dx, int dy) override
        {
            for(const auto& s : shapes_)
                s->move(dx, dy);
        }

        void draw() const override
        {
            for(const auto& s : shapes_)
                s->draw();
        }

        ShapeIterator begin() const
        {
            return shapes_.begin();
        }

        ShapeIterator end() const
        {
            return shapes_.end();
        }

        size_t size() const
        {
            return shapes_.size();
        }

        std::unique_ptr<IShape> clone() const override
        {
            return std::make_unique<ShapeGroup>(*this);
        }

        void accept(ShapeVisitor& v) override
        {
            v.visit(*this);
        }
    };

    class ShapeGroupReaderWriter : public IShapeReaderWriter
    {
        // IShapeReaderWriter interface
    public:
        void read(std::istream &in, IShape &shape) override
        {
            ShapeGroup& sg = dynamic_cast<ShapeGroup&>(shape);

            int count;
            in >> count;

            for(int i = 0; i < count; ++i)
            {
                std::string id;
                in >> id;
                auto shape = SingletonShapeFactory::instance().create(id);
                auto shape_rw = SingletonShapeRWFactory::instance().create(id);
                shape_rw->read(in, *shape);
                sg.add_shape(std::move(shape));
            }
        }

        void write(std::ostream &out, IShape &shape) override
        {
            ShapeGroup& sg = dynamic_cast<ShapeGroup&>(shape);

            out << "ShapeGroup " << sg.size() << std::endl;

//            for(const auto& s : sg)
//                s->write(out, *s);
        }
    };
}

#endif // SHAPEGROUP_H
