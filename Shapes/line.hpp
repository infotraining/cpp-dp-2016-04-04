#ifndef LINE_HPP
#define LINE_HPP

#include "shape.hpp"
#include "shape_reader_writer.hpp"
#include "visitor.hpp"

namespace Drawing
{
    class Line : public ShapeBase
    {
        Point end_;
    public:
        Line(int x1 = 0, int y1 = 0, int x2 = 0, int y2 = 0) : ShapeBase{x1, y1}, end_{x2, y2}
        {}

        Point end() const
        {
            return end_;
        }

        void set_end(Point end)
        {
            end_ = end;
        }

        void move(int dx, int dy) override
        {
            ShapeBase::move(dx, dy);
            end_.x += dx;
            end_.y += dy;
        }

        void draw() const override
        {
            std::cout << "Drawing line from " << coord() << " to " << end_ << std::endl;
        }

        std::unique_ptr<IShape> clone() const override
        {
            return std::make_unique<Line>(*this);
        }

        void accept(ShapeVisitor& v) override
        {
            v.visit(*this);
        }
    };

    class LineReaderWriter : public IShapeReaderWriter
    {
        // IShapeReaderWriter interface
    public:
        void read(std::istream &in, IShape &shape) override
        {
            Point pt1, pt2;

            in >> pt1 >> pt2;

            Line& l = dynamic_cast<Line&>(shape);
            l.set_coord(pt1);
            l.set_end(pt2);
        }

        void write(std::ostream &out, IShape &shape) override
        {
            Line& l = dynamic_cast<Line&>(shape);
            out << "Line " << l.coord() << " " << l.end() << std::endl;
        }
    };
}

#endif // LINE_HPP
