#ifndef SQUARE_HPP
#define SQUARE_HPP

#include "shape.hpp"
#include "rectangle.hpp"
#include "shape_reader_writer.hpp"

namespace Drawing
{
    class Square : public IShape
    {
        using Rect = Drawing::Rectangle;
        Rect r_;
    public:
        Square(int x = 0, int y = 0, int size = 0) : r_{x, y, size, size}
        {}

        int size() const
        {
            return r_.width();
        }

        void set_size(int size)
        {
            r_.set_width(size);
            r_.set_height(size);
        }

        Point coord() const
        {
            return r_.coord();
        }

        void set_coord(const Point& coord)
        {
            r_.set_coord(coord);
        }

        void move(int dx, int dy)
        {
            r_.move(dx, dy);
        }

        void draw() const override
        {
            r_.draw();
        }

        int area() const
        {
            return r_.area();
        }

        std::unique_ptr<IShape> clone() const override
        {
            return std::make_unique<Square>(*this);
        }

        void accept(ShapeVisitor& v) override
        {
            v.visit(*this);
        }
    };

    class SquareReaderWriter : public IShapeReaderWriter
    {
    public:
        void read(std::istream &in, IShape &shape) override
        {
            Point pt;
            int size;
            in >> pt >> size;

            Square& s = dynamic_cast<Square&>(shape);
            s.set_coord(pt);
            s.set_size(size);
        }

        void write(std::ostream &out, IShape &shape) override
        {
            Square& s = dynamic_cast<Square&>(shape);

            out << "Square ";
            out << s.coord() << " " << s.size();
        }
    };
}
#endif // SQUARE_HPP
