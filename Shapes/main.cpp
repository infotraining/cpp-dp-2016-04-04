#include <iostream>
#include <vector>
#include <memory>

#include "shape.hpp"
#include "shape_group.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "square.hpp"
#include "graphics_document.hpp"

using namespace std;
using namespace Drawing;

class AreaVisitor : public ShapeVisitor
{
    double area_ = 0;

    // ShapeVisitor interface
public:
    void visit(Circle &c) override
    {
        area_ += c.radius() * c.radius() * 3.14;
    }

    void visit(Line &l) override
    {
    }

    void visit(Rectangle &r) override
    {
        area_ += r.width() * r.height();
    }

    void visit(Square &s) override
    {
        area_ += s.area();
    }

    void visit(ShapeGroup &sg) override
    {
        for(auto& s : sg)
            s->accept(*this);
    }

    double area() const
    {
        return area_;
    }
};

int main() try
{
//    ShapeFactory shape_factory;
//    bootstrap_shape_factory(shape_factory);

//    ShapeRWFactory shape_rw_factory;
//    bootstrap_shape_rw_factory(shape_rw_factory);

    PlainTextShapeParser parser(SingletonShapeFactory::instance(), SingletonShapeRWFactory::instance());
    GraphicsDocument doc(parser);
    doc.open("drawing.txt");
    doc.show();

    AreaVisitor area_visitor;
    doc.visit(area_visitor);

    cout << "area = " << area_visitor.area() << endl;


}
catch(const exception& e)
{
    cout << "Exception caught: " << e.what() << endl;
}
