#ifndef SHAPE_FACTORY_HPP
#define SHAPE_FACTORY_HPP

#include <unordered_map>
#include "circle.hpp"
#include "rectangle.hpp"
#include "square.hpp"
#include "shape_reader_writer.hpp"
#include "generic_factory.hpp"
#include "singleton_holder.hpp"

namespace Drawing
{
    class IShapeFactory
    {
    public:
        virtual ~IShapeFactory() = default;
        virtual ShapePtr create(const std::string& id) = 0;
    };

    class ShapeFactory : public IShapeFactory, public GenericFactory<ShapePtr>
    {
    public:
        ShapePtr create(const std::string& id) override
        {
            return create_object(id);
        }
    };

    using SingletonShapeFactory = SingletonHolder<ShapeFactory>;

    class ShapeRWFactory : public IShapeReaderWriterFactory, public GenericFactory<ShapeReaderWriterPtr>
    {
    public:
        ShapeReaderWriterPtr create(const std::string &id) override
        {
            return create_object(id);
        }
    };

    using SingletonShapeRWFactory = SingletonHolder<ShapeRWFactory>;

    inline void bootstrap_shape_factory(ShapeFactory& f)
    {
        f.register_creator("Circle", [] { return std::make_unique<Circle>();});
        f.register_creator("Rectangle", [] {return std::make_unique<Rectangle>();});
        f.register_creator("Square", [] {return std::make_unique<Square>();});
    }

    inline void bootstrap_shape_rw_factory(ShapeRWFactory& f)
    {
        f.register_creator("Circle", [] { return std::make_unique<CircleReaderWriter>();});
        f.register_creator("Rectangle", [] {return std::make_unique<RectangleReaderWriter>();});
        f.register_creator("Square", [] {return std::make_unique<SquareReaderWriter>();});
    }
}

#endif // SHAPE_FACTORY_HPP
