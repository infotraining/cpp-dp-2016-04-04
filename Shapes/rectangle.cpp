#include "rectangle.hpp"
#include "shape_factory.hpp"

namespace
{
    using namespace Drawing;

    bool is_factory_registered =
            SingletonShapeFactory::instance()
                .register_creator("Rectangle", [] { return std::make_unique<Rectangle>();});

    bool is_rw_factory_registered =
            SingletonShapeRWFactory::instance()
                .register_creator("Rectangle", [] { return std::make_unique<RectangleReaderWriter>();});
}
