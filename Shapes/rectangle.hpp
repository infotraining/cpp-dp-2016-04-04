#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "shape.hpp"
#include "shape_reader_writer.hpp"
#include "visitor.hpp"

namespace Drawing
{
    class Rectangle : public ShapeBase
    {
    protected:
        int w_;
        int h_;
    public:
        Rectangle(int x = 0, int y = 0, int w = 0, int h = 0) : ShapeBase{x, y}, w_{w}, h_{h}
        {}

        virtual int width() const
        {
            return w_;
        }

        void set_width(int w)
        {
            w_ = w;
        }

        int height() const
        {
            return h_;
        }

        void set_height(int h)
        {
            h_ = h;
        }

        int area() const
        {
            return w_ * h_;
        }

        void draw() const override
        {
            std::cout << "Drawing a rect at " << coord() << " with w = " << w_ << " and h = " << h_ << std::endl;
        }

        std::unique_ptr<IShape> clone() const override
        {
            return std::make_unique<Rectangle>(*this);
        }

        void accept(ShapeVisitor& v) override
        {
            v.visit(*this);
        }
    };

    class RectangleReaderWriter : public IShapeReaderWriter
    {
    public:
        void read(std::istream &in, IShape &shape) override
        {
            Point pt;
            int w, h;
            in >> pt >> w >> h;

            Rectangle& r = dynamic_cast<Rectangle&>(shape);
            r.set_coord(pt);
            r.set_width(w);
            r.set_height(h);
        }

        void write(std::ostream &out, IShape &shape) override
        {
            Rectangle& r = dynamic_cast<Rectangle&>(shape);

            out << "Rectangle ";
            out << r.coord() << " " << r.width() << " " << r.height();
        }
    };
}

#endif // RECTANGLE_HPP
