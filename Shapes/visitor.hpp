#ifndef VISITOR_HPP
#define VISITOR_HPP

namespace Drawing
{
    class Circle;
    class Line;
    class Rectangle;
    class Square;
    class ShapeGroup;

    class ShapeVisitor
    {
    public:
        virtual void visit(Circle& c) = 0;
        virtual void visit(Line& l) = 0;
        virtual void visit(Rectangle& r) = 0;
        virtual void visit(Square& s) = 0;
        virtual void visit(ShapeGroup& sg) = 0;
        virtual ~ShapeVisitor() = default;
    };
}

#endif // VISITOR_HPP
