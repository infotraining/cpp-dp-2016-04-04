#include "square.hpp"
#include "shape_factory.hpp"

namespace
{
    using namespace Drawing;

    bool is_factory_registered =
            SingletonShapeFactory::instance()
                .register_creator("Square", [] { return std::make_unique<Square>();});

    bool is_rw_factory_registered =
            SingletonShapeRWFactory::instance()
                .register_creator("Square", [] { return std::make_unique<SquareReaderWriter>();});
}
