#ifndef GRAPHICS_DOCUMENT_HPP
#define GRAPHICS_DOCUMENT_HPP

#include "shape.hpp"
#include "shape_parser.hpp"

namespace Drawing
{
    class GraphicsDocument
    {
        std::vector<ShapePtr> shapes_;
        IShapeParser& parser_;
    public:
        GraphicsDocument(IShapeParser& parser) : parser_{parser}
        {}

        void add_shape(ShapePtr shape)
        {
            shapes_.push_back(std::move(shape));
        }

        void open(const std::string& file_name)
        {
            shapes_ = parser_.parse(file_name);
        }

        void show() const
        {
            for(const auto& s : shapes_)
                s->draw();
        }

        size_t size() const
        {
            return shapes_.size();
        }

        void visit(ShapeVisitor& v)
        {
            for(auto& s : shapes_)
                s->accept(v);
        }
    };
}
#endif // GRAPHICS_DOCUMENT_HPP
