#ifndef CLASS_SHAPE_HPP
#define CLASS_SHAPE_HPP

#include <iostream>
#include <memory>
#include "point.hpp"

namespace Drawing
{

class ShapeVisitor;

class IShape
{
public:
    virtual ~IShape() = default;
    virtual void move(int dx, int dy) = 0;
    virtual void draw() const = 0;
    virtual void accept(ShapeVisitor& v) = 0;
    virtual std::unique_ptr<IShape> clone() const = 0;
};

using ShapePtr = std::unique_ptr<IShape>;

class ShapeBase : public IShape
{
    Point coord_;

public:
    ShapeBase(int x = 0, int y = 0) : coord_{x, y}
    {}

    Point coord() const
    {
        return coord_;
    }

    void set_coord(const Point& coord)
    {
        coord_ = coord;
    }

    void move(int dx, int dy) override
    {
        coord_.x += dx;
        coord_.y += dy;
    }
};

}

#endif //CLASS_TEMPLATES_VECTOR_HPP
