#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "shape.hpp"
#include "shape_reader_writer.hpp"
#include "visitor.hpp"

namespace Drawing
{
    class Circle : public ShapeBase
    {
        int radius_;
    public:
        Circle(int x = 0, int y = 0, int r = 0) : ShapeBase{x, y}, radius_{r}
        {}

        int radius() const
        {
            return radius_;
        }

        void set_radius(int r)
        {
            radius_ = r;
        }

        void draw() const override
        {
            std::cout << "Drawing circle at " << coord() << " with r = " << radius_ << std::endl;
        }

        std::unique_ptr<IShape> clone() const override
        {
            return std::make_unique<Circle>(*this);
        }

        void accept(ShapeVisitor& v) override
        {
            v.visit(*this);
        }
    };

    class CircleReaderWriter : public IShapeReaderWriter
    {
    public:
        void read(std::istream &in, IShape &shape) override
        {
            Point pt;
            int r;

            in >> pt >> r;

            Circle& c = dynamic_cast<Circle&>(shape);

            c.set_coord(pt);
            c.set_radius(r);
        }

        void write(std::ostream &out, IShape &shape) override
        {
            Circle& c = dynamic_cast<Circle&>(shape);

            out << "Circle ";
            out << c.coord() << " " << c.radius();
        }
    };
}

#endif // CIRCLE_HPP
