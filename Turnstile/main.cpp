#include <iostream>
#include "turnstile.hpp"

using namespace std;

int main()
{
    TurnstileAPI api;

    After::Turnstile t{api};

    t.coin();
    t.pass();
    t.pass();
    t.coin();
    t.pass();
    t.coin();
    t.coin();
    t.coin();
}
