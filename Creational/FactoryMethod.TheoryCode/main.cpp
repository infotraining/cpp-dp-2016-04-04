#include <iostream>
#include <cstdlib>
#include <vector>
#include <map>
#include <string>

#include "factory.hpp"

using namespace std;

class Client
{
    shared_ptr<ServiceCreator> creator_;
public:
    Client(shared_ptr<ServiceCreator> creator) : creator_(creator)
	{
	}

    Client(const Client&) = delete;
    Client& operator=(const Client&) = delete;

    void use()
	{
        auto service = creator_->create_service();
        string result = service->run();
        cout << "Client is using: " << result << endl;        
	}
};

int main()
{
    typedef std::map<std::string, shared_ptr<ServiceCreator>> Factory;
    Factory creators;
    creators.insert(make_pair("A", make_shared<ConcreteCreatorA>()));
    creators.insert(make_pair("B", make_shared<ConcreteCreatorB>()));
    creators.insert(make_pair("C", make_shared<ConcreteCreatorC>()));

    Client client(creators["C"]);
	client.use();

    Client client2(creators["A"]);
    client2.use();

    // iterator with factory method
    set<int> vec = { 1, 2, 3, 4, 5, 6 };

    for(auto it = vec.begin(); it != vec.end(); ++it)
    {
        cout << *it << " ";
    }
}
