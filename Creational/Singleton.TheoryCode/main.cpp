#include <iostream>
#include "singleton.hpp"

using namespace std;
using WithHolder::Log;

class Service
{
public:
    void run()
    {
        get_logger().write("Service is started");

        // ... impl
    }
protected:
    virtual WithHolder::LogSink& get_logger() const
    {
        return Log::instance();
    }
};

class MockLogger : public WithHolder::LogSink
{
};

class TestableService
{
    MockLogger log_;
protected:
    virtual WithHolder::LogSink& get_logger()
    {
        return log_;
    }
};

int main()
{
    using WithHolder::Log;

    Log::instance().write("Starting main...");

	Singleton::instance().do_something();

	Singleton& singleObject = Singleton::instance();
	singleObject.do_something();

    Log::instance().write("End of main...");
}
