#ifndef SINGLETON_HPP_
#define SINGLETON_HPP_

#include <iostream>

class Singleton
{
public:
    Singleton(const Singleton&) = delete;
    Singleton& operator=(const Singleton&) = delete;

	static Singleton& instance()
	{
        static Singleton unique_instance;

        return unique_instance;
	}

	void do_something();

private:   
	Singleton() // uniemozliwienie klientom tworzenie nowych singletonow
	{ 
		std::cout << "Constructor of singleton" << std::endl; 
	} 

	~Singleton() // prywatny destruktor chroni przed wywolaniem delete dla adresu instancji
	{ 
		std::cout << "Singleton has been destroyed!" << std::endl;
	} 
};

void Singleton::do_something()
{
	std::cout << "Singleton instance at " << std::hex << &instance() << std::endl;
}


namespace WithoutHolder
{

    class Log
    {
    public:
        Log(const Log&) = delete;
        Log& operator =(const Log&) = delete;

        void write(const std::string& msg)
        {
            std::cout << "Logging: " << msg << std::endl;
        }

        static Log& instance()
        {
            static Log unique_logger;
            return unique_logger;
        }

    private:
        Log() = default;
    };

}

template <typename T>
class SingletonHolder
{
public:
    SingletonHolder(const SingletonHolder&) = delete;
    SingletonHolder& operator=(const SingletonHolder&) = delete;

    static T& instance()
    {
        static T unique_instance;

        return unique_instance;
    }
private:
    SingletonHolder() = default;
};

namespace WithHolder
{
    class LogSink
    {
    public:
        void write(const std::string& msg)
        {
            std::cout << "Logging: " << msg << std::endl;
        }
    };

    using Log = SingletonHolder<LogSink>;
}

#endif /*SINGLETON_HPP_*/
