get_filename_component(ProjectId ${CMAKE_SOURCE_DIR} NAME)
string(REPLACE " " "_" ProjectId ${ProjectId})
project(${ProjectId})
cmake_minimum_required(VERSION 2.8)

#----------------------------------------
# set Boost
#----------------------------------------
set(Boost_USE_STATIC_LIBS   ON)
set(Boost_USE_MULTITHREADED ON)

find_package(Boost 1.50.0 REQUIRED)
if(Boost_FOUND)
    include_directories(${Boost_INCLUDE_DIRS})
endif()

aux_source_directory(. SRC_LIST)
add_executable(${PROJECT_NAME} ${SRC_LIST})
ADD_DEFINITIONS("-std=c++0x")
target_link_libraries( ${PROJECT_NAME} stdc++)
file(GLOB libs_SRC "*.h" "*.hpp")
add_custom_target(libs SOURCES ${libs_SRC})
