#include <iostream>
#include "led_light.hpp"

using namespace std;

int main()
{
    LEDSwitchAsClassAdapter l;
    Button btn{l};
    btn.click();

    LEDLight led_light;
    LEDSwitchAsObjectAdapter adapted_led{led_light};
    Button btn2{adapted_led};
    btn2.click();
}
