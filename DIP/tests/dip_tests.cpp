#define CATCH_CONFIG_MAIN

#include <algorithm>
#include "catch.hpp"
#include "../led_light.hpp"

using namespace std;

class MockLEDLight : public ILEDLight
{
public:
    int r, g, b = {};

    void set_rgb(int r, int g, int b)
    {
        this->r = r;
        this->g = g;
        this->b = b;
    }
};

class MockSwitch : public ISwitch
{    // ISwitch interface
public:
    int on_called = 0;
    int off_called = 0;

    void on() override
    {
        on_called++;
    }

    void off() override
    {
        off_called++;
    }
};

TEST_CASE("Button")
{
    SECTION("click sets on the light")
    {
        MockSwitch mq_switch;
        Button btn{mq_switch};

        btn.click();

        REQUIRE(mq_switch.on_called == 1);
    }
}





