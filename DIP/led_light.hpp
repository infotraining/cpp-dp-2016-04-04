#include <iostream>

// low level

class ILEDLight
{
public:
    virtual ~ILEDLight() = default;
    virtual void set_rgb(int r, int g, int b) = 0;
};

class LEDLight : public ILEDLight
{
public:
    void set_rgb(int r, int g, int b)
    {
        std::cout << "Light set to (" << r << ", " << g << ", " << b << ")" << std::endl;
    }
};


// high level
class ISwitch
{
public:
    virtual ~ISwitch() = default;
    virtual void on() = 0;
    virtual void off() = 0;
};

// class adapter
class LEDSwitchAsClassAdapter : public ISwitch, private LEDLight
{
public:
    void on() override
    {
        set_rgb(255, 255, 255);
    }

    void off() override
    {
        set_rgb(0, 0, 0);
    }
};

class LEDSwitchAsObjectAdapter : public ISwitch
{
    ILEDLight& l_;
public:
    LEDSwitchAsObjectAdapter(ILEDLight& l) : l_{l}
    {}

    void on() override
    {
        l_.set_rgb(255, 255, 255);
    }

    void off() override
    {
        l_.set_rgb(0, 0, 0);
    }
};


// high level
class Button
{
    ISwitch& l_;
public:
    Button(ISwitch& light) : l_{light}
    {}

    void click()
    {
        l_.on();
    }
};
